import Banner from '../components/Banner';

export default function NotFound() {

	const data = {
		title: "Not Found",
		content: "Page is broken.",
		destination: "/",
		label: "Back home"
	}

	return (
		<Banner data={data}/>
	)
}

