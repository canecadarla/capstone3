import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.access !== undefined){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Login successful!"
                });
            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }
        })

        setEmail('');
        setPassword('');
    }


    // Retrieving user details

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        if ((email !== '' && password !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

        }, [email, password]);

    return (

        (user.isAdmin === true)
        ?
            <Navigate to='/admin'/>
        :
            (user.id !== null)
            ?
                <Navigate to='/products' />
            :
        
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId = "userEmail">
                <Form.Label>Email Address</Form.Label>
                <Form.Control 
                    type="email"
                    placeholder="Enter email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId = "password">
                <Form.Label>Password</Form.Label>
                <Form.Control                   
                    type="password"
                    placeholder="Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group>       
            {isActive
                ?
                    <Button variant="primary" type="submit" id="submitBtn">Login</Button>
                :   <Button variant="primary" type="submit" id="submitBtn" disabled>Login</Button>
            }
        </Form>
    )
}
