import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');	

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);


	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})

			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data === true){
						Swal.fire({
							title: "Registration Successful!",
							icon: "success",
							text: "Welcome to DM Shop!"
						})

						// Clear input fields
						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword1('');
						setPassword2('');

					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please use another email."
						})
					};
					navigate("/login");
				})
			}
		})	
	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' & password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2]);

	return	(
		(user.id !== null)
		?
			<Navigate to="/products"/>
		:

				<Form onSubmit={(e) => registerUser(e)}>
					<Form.Group controlId="firstName">
					    <Form.Label>First Name</Form.Label>
					    <Form.Control 
					        type="text" 
					        placeholder="Enter first name"			        
					        required
					        value={firstName} 
					        onChange={e => setFirstName(e.target.value)}
					    />
					</Form.Group>

					<Form.Group controlId="lastName">
					    <Form.Label>Last Name</Form.Label>
					    <Form.Control 
					        type="text" 
					        placeholder="Enter last name"			        
					        required
					        value={lastName} 
					        onChange={e => setLastName(e.target.value)}
					    />
					</Form.Group>

					<Form.Group controlId="userEmail">
						<Form.Label>Email Address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter email"
							required
							value={email}
							onChange={e => setEmail(e.target.value)}
						/>
						<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>			
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Password"
							required
							value={password1}
							onChange={e => setPassword1(e.target.value)}
						/>			
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Verify Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Verify password"
							required
							value={password2}
							onChange={e => setPassword2(e.target.value)}
						/>		
					</Form.Group>

					{isActive	
						?
							<Button variant	="primary" type="submit" id="submitBtn">Submit</Button>
						:
							<Button variant	="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
					}
					
				</Form>
			)
		}
