import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';


export default function Home() {

	const data = {
		title: "DM Shop",
		description: "Ready-to-eat and drink snacks",
		destination: "/products",
		label: "Buy now!"
	}
	return (
		<>
			<Container>
	          <Banner data={data}/>          
        	</Container>
        </>
	)
}
