import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import AdminDashboard from './pages/AdminDashboard';
import './App.css';
import { UserProvider } from './UserContext';


function App() {
    
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        console.log(user);
        console.log(localStorage);
    }, [user])

  return(
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
            <AppNavbar/>
            {/*<Home/>*/}
            <Container>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/login" element={<Login/>} />
                    <Route path="/register" element={<Register/>} />
                    <Route path="/logout" element={<Logout/>} />
                    <Route path="*" element={<NotFound/>} />
                    <Route path="/products" element={<Products/>} />
                    <Route path="/products/:productId" element={<ProductView/>} />
                    <Route path="/admin" element={<AdminDashboard/>} />
                </Routes>
            </Container>         
        </Router>
    </UserProvider>
  );
}

export default App;
