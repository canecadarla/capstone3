import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

	const { title, description, destination, label } = data;	

	return (		
			<Row>
				<Col className="p-5 text-center">
					<h1>{title}</h1>
					<p>{description}</p>
					<Button as={Link} to={destination} variant="primary">{label}</Button>
				</Col>
			</Row>
		
	)
}

