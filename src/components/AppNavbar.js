import { useContext } from 'react';
import { Navbar, Container, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return(
    <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/">DM Shop</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to="/">Home</Nav.Link>

              {
                (user.isAdmin)
                ?
                <Nav.Link as={ NavLink } to="/admin" end>Admin Dashboard</Nav.Link>
                :
                <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
              }

              {(user.id !== null)

                ?
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link> 
                </>
              }

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
  )
}
